Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "welcome#index"
  get '/login', to: 'login#index'
  get '/reset_password/:id', to: 'user#reset_password'
  get '/welcome', to: 'welcome#index'
  get '/user/list/:page', to: 'user#index'
  get '/user/new', to: 'user#new'
  get '/user/edit/:id', to: 'user#edit'
  get '/user/setting/:id', to: 'user#setting'
  get '/suggest/new', to: 'suggest_place#new'
  get '/suggest/edit/:id', to: 'suggest_place#edit'
  get '/vote/list', to: 'vote#index'
  get '/suggest/list/:page', to: 'suggest_place#index'
  get '/logout', to: 'login#logout'
  post "/login/validate"
  post "/user/create"
  post "/user/update"
  post "/suggest/create", to: 'suggest_place#create'
  post "/suggest/update", to: 'suggest_place#update'
  get "/suggest/delete/:id", to: 'suggest_place#delete'
  post "/user/setprofile"
  get "/user/delete/:id", to: 'user#delete'
  get "/user/reset/:id", to: 'user#request_reset_password'
  get "/suggest/check/:is_used/:id", to: 'suggest_place#check'
  get "/random/place", to: 'vote#random'
end
