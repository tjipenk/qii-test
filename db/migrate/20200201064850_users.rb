class Users < ActiveRecord::Migration[6.0]
  def up 
    create_table :users do |t|
      t.column :password,:string
      t.column :name,:string
      t.column :email,:string
      t.column :role,:string
      t.column :reset_password,:string, :limit=>1,:null=>true
      t.column :active,:string, :limit=>1
      t.column :created_at,:datetime
      t.column :created_by,:string
      t.column :updated_at,:datetime,:null=>true
      t.column :updated_by,:string,:null=>true
    end
    User.create :password => "Manager", :name => "Manager", :email => "manager@manager.com", :role => "Manager", :reset_password => "1", :active => "1", :created_at => "2020-02-01 00:00:00", :created_by => "Developer", :updated_at => "2020-02-01 00:00:00", :updated_by => "Developer"
  end
  def down
    drop_table :users
  end
end
