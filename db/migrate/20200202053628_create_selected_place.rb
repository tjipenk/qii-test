class CreateSelectedPlace < ActiveRecord::Migration[6.0]
  def up
    create_table :selected_places do |t|
      t.column :active,:string, :limit=>1
      t.column :created_at,:datetime
      t.column :created_by,:string
      t.column :updated_at,:datetime,:null=>true
      t.column :updated_by,:string,:null=>true
    end
    add_reference :selected_places, :suggest_places, foreign_key: true
  end
  def down
    drop_table :selected_places
  end
end
