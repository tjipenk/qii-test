class CreateSuggestPlace < ActiveRecord::Migration[6.0]
  def up
    create_table :suggest_places do |t|
      t.column :name,:string
      t.column :address,:text
      t.column :is_used,:string, :limit=>1
      t.column :active,:string, :limit=>1
      t.column :created_at,:datetime
      t.column :created_by,:string
      t.column :updated_at,:datetime,:null=>true
      t.column :updated_by,:string,:null=>true
    end
    add_reference :suggest_places, :users, foreign_key: true

  end
  def down
    drop_table :suggest_places
  end
end
