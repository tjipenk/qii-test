# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_02_053628) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "selected_places", force: :cascade do |t|
    t.string "active", limit: 1
    t.datetime "created_at"
    t.string "created_by"
    t.datetime "updated_at"
    t.string "updated_by"
    t.bigint "suggest_places_id"
    t.index ["suggest_places_id"], name: "index_selected_places_on_suggest_places_id"
  end

  create_table "suggest_places", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "is_used", limit: 1
    t.string "active", limit: 1
    t.datetime "created_at"
    t.string "created_by"
    t.datetime "updated_at"
    t.string "updated_by"
    t.bigint "users_id"
    t.index ["users_id"], name: "index_suggest_places_on_users_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "password"
    t.string "name"
    t.string "email"
    t.string "role"
    t.string "reset_password", limit: 1
    t.string "active", limit: 1
    t.datetime "created_at"
    t.string "created_by"
    t.datetime "updated_at"
    t.string "updated_by"
  end

  add_foreign_key "selected_places", "suggest_places", column: "suggest_places_id"
  add_foreign_key "suggest_places", "users", column: "users_id"
end
