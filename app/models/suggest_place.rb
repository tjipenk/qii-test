class SuggestPlace < ApplicationRecord
    belongs_to :users, foreign_key: :users_id,class_name: 'User'
    has_one :selected_place
end
