class User < ApplicationRecord
    has_many :suggest_place
    validates_presence_of :email
end
