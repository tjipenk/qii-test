class SelectedPlace < ApplicationRecord
    has_one :suggest_place, foreign_key: :suggest_place_id,class_name: 'SuggestPlace'
end
