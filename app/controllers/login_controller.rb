class LoginController < ApplicationController
    skip_before_action :required_login
    def index
    end
    
    def validate
        @user = User.find_by(email: params[:email],password: params[:password],active:"1")
        if @user
            session[:user_id]=@user.id
            session[:user_email]=@user.email
            session[:user_name]=@user.name
            session[:user_role]=@user.role
            if @user.reset_password == "1"
                redirect_to '/reset_password/'+session[:user_id].to_s
            else
                redirect_to '/welcome'
            end
        else
            redirect_to '/login'
        end
    end
    def logout
        reset_session
        redirect_to '/login'
    end
end
