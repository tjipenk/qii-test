class VoteController < ApplicationController
    def index
        @title = "Porpose Place"
        @curr_page= "vote"
        @selected = SelectedPlace.where("DATE(created_at)=? and active=?",Date.today,"1")
        @list = SuggestPlace.select("suggest_places.*,users.name as user_name").joins("inner join users on users.id=suggest_places.users_id").where("suggest_places.active=? AND is_used=? AND (DATE(suggest_places.created_at)=? OR DATE(suggest_places.updated_at)=?)","1","1",Date.today,Date.today)
    end
    def random
        list = SuggestPlace.where("suggest_places.active=? AND is_used=? AND (DATE(suggest_places.created_at)=? OR DATE(suggest_places.updated_at)=?)","1","1",Date.today,Date.today)
        count = list.count()
        num_rand=rand(count)
        puts num_rand
        qselected = SelectedPlace.where("DATE(created_at)=? and active=?",Date.today,"1")
        if qselected.count()==0
            selected = SelectedPlace.new(suggest_places_id: list[num_rand].id,active: "1",created_at: Time.now,created_by: session[:user_name])
            selected.save
        else
            selected = SelectedPlace.find(qselected[0].id)
            selected.update_attributes(suggest_places_id: list[num_rand].id,created_by: session[:user_name],created_at: Time.now)
        end
        redirect_to "/vote/list"
    end
end
