class ApplicationController < ActionController::Base
    before_action :required_login
    def required_login
        if session[:user_id] == nil
            redirect_to "/login"
        end
    end
end
