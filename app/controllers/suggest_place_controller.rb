class SuggestPlaceController < ApplicationController
    def index 
        @title = "Suggest Place"
        @curr_page= "suggest"
        @update = SuggestPlace.where("DATE(updated_at) < ? AND DATE(created_at) < ? AND users_id = ?",Date.today,Date.today,session[:user_id])
        @update.update_all(is_used: "0",updated_by: session[:user_name],updated_at: Time.now)
        @limit = 10
        @page = params[:page].to_f-1
        @count_page = (SuggestPlace.all.count().to_f/@limit).ceil
        @list_page= Array(1..@count_page)
        @list = SuggestPlace.where(users_id: session[:user_id],active:"1").limit(@limit).offset((@page*@limit))
    end
    def new
        @title = "Suggest Form New"
    end
    def edit
        @title = "Suggest Form Edit"
        @id = params[:id]
        @suggestplace = SuggestPlace.find(params[:id])
    end
    def create
        @suggestplace = SuggestPlace.new(name: params[:name],address: params[:address],is_used: "1", active: "1",created_by: session[:user_name],created_at: Time.now,users_id: session[:user_id])
        if @suggestplace.save
            @user = User.find_by(role: "Manager")
            CreateSuggestMailer.msg(@user,@suggestplace).deliver
            redirect_to "/suggest/list/1"
        else
            redirect_to "/suggest/new"
        end
    end
    def update
        @suggestplace = SuggestPlace.find(params[:id])
        if @suggestplace.update_attributes(name: params[:name], address: params[:address],is_used: "1",updated_by: session[:user_name],updated_at: Time.now)
            redirect_to "/suggest/list/1"
        else
            redirect_to "/suggest/edit/"+params[:id]
        end
    end
    def delete
        @suggestplace = SuggestPlace.find(params[:id])
        @suggestplace.update_attributes(active: "0",updated_by: session[:user_name],updated_at: Time.now)
        redirect_to "/suggest/list/1"
    end
    def check
        @suggestplace = SuggestPlace.find(params[:id])
        @suggestplace.update_attributes(is_used: params[:is_used],updated_by: session[:user_name],updated_at: Time.now)
        redirect_to "/suggest/list/1"
    end
end
