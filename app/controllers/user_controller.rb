class UserController < ApplicationController
    skip_before_action :required_login,:only=>[:reset_password,:set_profile]
    def index
        @title = "Management User"
        @curr_page= "user"
        @limit = 10
        @page = params[:page].to_f-1
        @count_page = (User.all.count().to_f/@limit).ceil
        @list_page= Array(1..@count_page)
        @list = User.where(active: "1").limit(@limit).offset((@page*@limit))
    end
    def reset_password
        @id = params[:id]
        @user = User.find(params[:id])
    end
    def new
        @title = "User Form New"
    end
    def create
        @user = User.new(name: params[:name],email: params[:email],password: params[:password],role: "Member",reset_password: "1",active: "1",created_by: session[:user_name],created_at: Time.now)
        if @user.save
            CreateUserMailer.msg(@user,request.base_url).deliver_now
            redirect_to "/user/list/1"
        else
            redirect_to "/user/new"
        end
    end
    def edit
        @title = "User Form Edit"
        @id = params[:id]
        @user = User.find(params[:id])
    end
    def setting
        @title = "User Form Edit"
        @id = params[:id]
        @user = User.find(params[:id])
    end
    def update
        @user = User.find(params[:id])
        if @user.update_attributes(email: params[:email], name: params[:name], password: params[:password], active: "1",updated_by: session[:user_name],updated_at: Time.now)
            @users = User.find(params[:id])
            CreateUserMailer.msg(@users,request.base_url).deliver_now
            redirect_to "/user/list/1"
        else
            redirect_to "/user/edit/"+params[:id]
        end
    end
    def setprofile
        @user = User.find(params[:id])
        @user.update_attributes(active: "1",reset_password: "0",updated_by: session[:user_name],updated_at: Time.now)
        if session[:user_id].present?
            redirect_to "/user/setting/"+params[:id]    
        else
            redirect_to "/logout"
        end
    end
    def delete
        @user = User.find(params[:id])
        @user.update_attributes(active: "0",updated_by: session[:user_name],updated_at: Time.now)
        redirect_to "/user/list/1"
    end
    def request_reset_password
        @user = User.find(params[:id])
        @user.update_attributes(reset_password: "1",updated_by: session[:user_name],updated_by: Time.now)
        redirect_to "/user/list/1"
    end
end
