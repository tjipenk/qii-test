class WelcomeController < ApplicationController
    def index
        @title = "Welcome Back "+session[:user_name]
    end
end
