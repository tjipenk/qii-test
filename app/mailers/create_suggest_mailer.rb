class CreateSuggestMailer < ApplicationMailer

    def msg(user,suggestplace)
        @user = user
        @suggestplace = suggestplace
        mail(to: @user.email, subject: '[do-not-reply] Add Suggest Place')
    end
end
