class CreateUserMailer < ApplicationMailer

    def msg(user,base_url)
        @user = user
        @base_url = base_url
        mail(to: @user.email, subject: '[do-not-reply] Adding User Login Apps')
    end
    def reset_password(user,base_url)
        @user = user
        @base_url = base_url
        mail(to: @user.email, subject: '[do-not-reply] Reset Data Login Apps')
    end
end
